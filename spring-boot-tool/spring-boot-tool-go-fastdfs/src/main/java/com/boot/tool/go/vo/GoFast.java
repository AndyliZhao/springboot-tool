package com.boot.tool.go.vo;

import lombok.*;

import java.util.Date;

/**
 * 文件系统返回数据
 * @author YI
 * @date 2019-1-29 10:48:11
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoFast {

    private String url;
    private String md5;
    private String path;
    private Date domain;
    private String scene;
    private String scenes;
    private String retmsg;
    private int retcode;
    private String src;
}
