package com.boot.tool.go.web;

import cn.hutool.core.util.StrUtil;
import com.boot.tool.common.result.CommonResult;
import com.boot.tool.go.service.GoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author: Andy Li
 * @Date: 2019/3/1 17:55
 * @Version 1.0.0
 */
@Scope("prototype")
@RestController
@RequestMapping("/app/setting")
public class GoController {

    @Autowired
    private GoService goService;
    /**
     * 上传图片
     * @param file
     * @return
     */
    @PostMapping("uploadImg")
    public CommonResult uploadImg(
            @RequestParam("file") MultipartFile file){
        String url = this.goService.uploadImage(file);
        if (StrUtil.isBlank(url)) {
            return CommonResult.fail("图片不符合格式");
        }
        return CommonResult.success(url);
    }

}
