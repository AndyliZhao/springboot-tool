package com.boot.tool.go;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Andy Li
 * @Date: 2019/3/1 17:53
 * @Version 1.0.0
 */
@SpringBootApplication
public class GoApplication {
    public static void main(String[] args) {
        SpringApplication.run(GoApplication.class,args);
    }
}
