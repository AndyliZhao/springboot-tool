package com.boot.tool.go.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @Author: Andy Li
 * @Date: 2019/2/14 13:22
 * @Version 1.0.0
 */
@Configuration
public class GoFastdfsConfig {
    /**
     * 文件系统url
     */
    @Value("${go.fastdfs.upload}")
    public  String upload;

    @Value("${go.fastdfs.delete}")
    public  String delete;

}
