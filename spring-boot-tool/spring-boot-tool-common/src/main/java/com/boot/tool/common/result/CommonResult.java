package com.boot.tool.common.result;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: Andy Li
 * @Date: 2019/3/11 16:38
 * @Version 1.0.0
 *
 */
@Getter
@Setter
public class CommonResult<T> {
    /**
     * 返回状态码
     */
    private String code;
    /**
     * 状态码描述
     */
    private String message;

    /**
     * 返回信息
     */
    private T data;

    public CommonResult(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<T>("success","请求成功",data);
    }

    public static <T> CommonResult<T> fail(T data) {
        return new CommonResult<T>("fail","请求失败",data);

    }

}
