package com.boot.tool.common.exception;


import lombok.Getter;
import lombok.Setter;

/**
 * @Author: Andy Li
 * @Date: 2019/3/11 16:41
 * @Version 1.0.0
 */
@Getter
@Setter
public class AppServiceException extends RuntimeException {


    private  String  code ;

    private  String  massage ;

    public AppServiceException(String code, String massage) {
        this.code = code;
        this.massage = massage;
    }

    public static AppServiceException SERVER_IO_FORMAT_EXCEPTION = new AppServiceException("100001","文件格式不正确");

    public static AppServiceException  SERVER_IO_WORK_EXCEPTION = new AppServiceException("100002","文件内容不合适");

    public static AppServiceException  SERVER_NO_FOUND_EXCEPTION = new AppServiceException("100003","未发现服务器");
}
